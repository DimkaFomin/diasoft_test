<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 13.03.2019
 * Time: 19:36
 */

class Chats{
    /*строка подключения к бд MySQL*/
    public $conn = 1;

    //создание комнат для переписки
    //0 - диалог(Д)
    //1 - многопользовательская переписка(МП)

    public function createChat($name, $id_type, $users_ids){
        //входные параметры: название Д или МП, тип (0/1),
        //массив с id пользователей, которые будут находится в комнате на
        //момент её создания

        //запрос на создание комнаты и его исполнение
        $sql = "INSERT INTO Rooms(name, id_type) VALUES ('".$name."', ".$id_type.")";
        $this->conn->mysqli_query($sql);

        //получаем id последней созданной комнаты
        $id_of_just_created_room = $this->conn->insert_id;

        //запросы на добавление пользователей в комнаты и их выполнение
        foreach ($users_ids as $user_id){
            $sql = "INSERT INTO Rooms_Users VALUES (".$user_id.", ".$id_of_just_created_room.", 1)";
            $this->conn->mysqli_query($sql);
        }

        //закрываем соединение с БД
        $this->conn->close();
    }

    //приглашение пользователя в беседу
    public function invitePerson($room_id, $newbie_in_chat){
        //входные параметры id комнаты и id пользователя, которого добавляют

        //запрос на выбор типа комнаты, его выполнение и извлечения данных
        $result = $this->conn->mysqli_query("SELECT id_type FROM Rooms WHERE id =".$room_id);
        $check_type = $result->fetch_assoc();
        $type = intval($check_type['id_type']);

        //запрос на проверку был ли человек в этой комнате раньше,
        //если да, просто ставим 1 в поле is_active
        //если нет, добавляем в таблицу новую запись
        $result = $this->conn->mysqli_query("SELECT is_active FROM Rooms_Users WHERE id_room =".$room_id."
                                            AND id_user =".$newbie_in_chat);
        $check_type = $result->fetch_assoc();

        if(count($check_type) == 0){
            //если тип комнаты МП то добавляем пользователя, в противном случае отправляем сообщение
            //(можно запредить функцию добавления людей на клинете,
            //но тут тоже обезапасимся условием)
            if($type == 1){
                $sql = "INSERT INTO Rooms_Users VALUES (".$newbie_in_chat.", ".$room_id.", 1)";
                $this->conn->mysqli_query($sql);
            }
            else{
                echo 'Ваша переписка рассчитана на 2х пользователей.';
            }
        }
        else{
            $this->conn->mysqli_query("UPDATE Rooms_Users SET is_active = 1 WHERE id_room =".$room_id."
                                            AND id_user =".$newbie_in_chat);
        }


    }


    //удаление человека из комнаты
    public function deletePersonFromChat($room_id, $user_id){
        //входные параметры: id комнаты и id пользователя, который уходит

        //переводим пользователя в данной комнате в статус "неактивен",
        //в связи с чем он перестаёт получать уведомления из этой комнаты
        //(на клиенте это будет расцениваться как удалён из беседы)

        $sql = "UPDATE Rooms_Users SET is_active = 0 
            WHERE id_user=".$user_id." and id_room=".$room_id;
        $this->conn->mysqli_query($sql);

    }

    //функция добавления сообщения в БД
    public function addMessage($message, $room_id){
        //входные параметры: текст сообщения и номер комнаты

        //запрос за создание записи в таблице Messages
        $sql = "INSERT INTO Messages(id_room, text) VALUES (" . $room_id . ", '" . $message . "')";
        $this->conn->mysqli_query($sql);

        //выбираем id последнего сообщения
        $last_inserted_message_id = $this->conn->insert_id;

        //запрос, который вернёт нам список активных пользователей в комнате
        $sql = "SELECT Rooms_Users.id_user from Rooms_User 
                JOIN Rooms on Rooms_Users.id_room = Rooms.id
                WHERE Rooms.id =" . $room_id . " 
                and Rooms_Users.is_active = 1";
        $result = $this->conn->mysqli_query($sql);
        $active_users_ids = $result->mysqli_fetch_array();

        //для каждого активного участника добавляем запись в таблицу Messages_Users
        //на клиенте новое сообщение увидят только те пользователи, которые в данный момент находятся в комнате
        foreach ($active_users_ids as $user_id) {
            $sql = "INSERT INTO Messages_Users VALUES(" . $last_inserted_message_id . "," . $user_id . ")";
            $this->conn->mysqli_query($sql);
        }
    }

    //удаление сообщения\
    public function deleteMessage($id_message, $id_user){
        //входные параметры: id сообщения, id пользователя, который удаляет сообщение

        //запрос на удаление пары id сообщения - id пользователя
        //в результате такого механизма выбранное сообщение удлаится только для конкретного пользоваотеля
        $sql = "DELETE FROM Messages_Users WHERE id_message =".$id_message." 
        and id_user=".$id_user;
        $this->conn->mysqli_query($sql);
    }

    //удаление комнаты
    public function deleteChat($room_id){
        //запрос на удаление записи из таблицы Rooms и его исполнение
        $sql = "DELETE FROM Rooms WHERE id=".$room_id;
        $this->conn->mysqli_query($sql);
    }


}
